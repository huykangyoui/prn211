﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Ass3.Models;

namespace Ass3.Pages.Car
{
    public class IndexModel : PageModel
    {
        private readonly Ass3.Models.MyStockContext _context;

        public IndexModel(Ass3.Models.MyStockContext context)
        {
            _context = context;
        }

        public IList<Ass3.Models.Car> Car { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Cars != null)
            {
                Car = await _context.Cars.ToListAsync();
            }
        }
    }
}
